from django.db import models


class Entry(models.Model):
    Author = models.CharField(max_length=50)
    Title = models.CharField(max_length=50)
    Text = models.CharField(max_length=500)
