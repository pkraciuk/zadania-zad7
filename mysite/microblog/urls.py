from django.conf.urls import patterns, url, include
from django.views.generic import TemplateView
import views

urlpatterns = patterns('',
    #ex: /blog/
    url(r'^$', views.index,name='index'),
    #ex: /blog/login/
    url(r'^login/$',TemplateView.as_view(template_name='microblog/login.html'),name='login'),
    #ex: /blog/add/
    #url(r'^add/$',TemplateView.as_view(template_name='microblog/add.html'),name='add'),
    url(r'^add/$',views.add,name='add'),
    #ex: /blog/added/
    url(r'^added/$',views.added,name='added'),
    #ex: /blog/logged/
    url(r'^logged/$',views.logged,name='logged'),
    #ex: /blog/logout/
    url(r'^logout/$',views.logout,name='logout'),
    #ex: /blog/myArticles/
    url(r'^myArticles/$',views.LoginUserArticles,name='myArticles'),

    #ex: /blog/myArticles/
    url(r'^error/$',views.Error,name='error'),


    #url(r'^vote/$', views.vote, name='vote')
    #ex: /blog/5
    #url(r'^(?P<microblog_id>\d+)/$',views.detail,name='detail'),
)
